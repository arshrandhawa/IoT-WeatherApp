package business;

import beans.User;
import util.UserAlreadyExistException;
import util.UserDoesNotExistException;

public interface UserManagerInterface {

	public boolean addUser(User user) throws UserAlreadyExistException;
	public User loginUser(User user) throws UserDoesNotExistException;
	public boolean checkNewUser(User user) throws UserAlreadyExistException;
}

package business;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.User;
import data.DataAccessInterface;
import util.LoggingInterceptor;
import util.UserAlreadyExistException;
import util.UserDoesNotExistException;

@Stateless
@Local(UserManagerInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class UserManager implements UserManagerInterface {

	@EJB(beanName = "UserDataService")
	DataAccessInterface<User> uds;

	//Create instance of SLF4J logger and name is based on the class name
	Logger logger = LoggerFactory.getLogger("UserManager LOGGER");

	/**
	 * This method calls the various methods to add a new user.
	 *
	 * @param User
	 *            returns boolean throws UserAlreadyExistException
	 * 
	 */
	@Override
	public boolean addUser(User user) throws UserAlreadyExistException {

		// check if the user is unique
		// if he is add them to the database other throw the exception that user
		// already exist

		if (!checkNewUser(user)) {

			if (uds.create(user)) {
				logger.info("Created user sucessfully");
				return true;
			}
		} else
			throw new UserAlreadyExistException();
		logger.info("Creating user failed");
		return false;
	}

	/**
	 * This method log the user in their account.
	 *
	 * @param User
	 *            returns User
	 */
	@Override
	public User loginUser(User user) throws UserDoesNotExistException {
		// find the user from the database
		User userNew = uds.findBy(user);
		logger.info("Finding user by user information");

		// if the user does not exist then throw the exception otherwiese return the
		// user
		if (userNew == null) {
			logger.info("Failed to find user");
			throw new UserDoesNotExistException();
		} else
			logger.info("Found user");
		return userNew;
	}

	/**
	 * this method will be used while registering if the user is already there or
	 * not
	 * 
	 * @param user
	 * @return User
	 * @throws UserAlreadyExistException 
	 */
	public boolean checkNewUser(User user) throws UserAlreadyExistException{

		User userNew = uds.findBy(user);
		logger.info("Finding user to see if he/she exist in database already");

		// if the user does not exist then throw the exception otherwiese return the
		// user
		if (userNew == null) {
			logger.info("User not in database");
			return false;
		} else {
			logger.info("User already in database");
			throw new UserAlreadyExistException();
		}

	}

}

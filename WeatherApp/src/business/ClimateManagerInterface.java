package business;

import java.util.List;

import beans.Climate;
import util.ClimateNotFoundException;

public interface ClimateManagerInterface {

	public boolean addWeather(Climate climate) throws ClimateNotFoundException;
	public List<Climate> getLastN(int n, Climate climate) throws ClimateNotFoundException;
}

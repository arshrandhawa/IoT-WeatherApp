package business;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.Climate;
import data.DataAccessInterface;
import util.ClimateNotFoundException;
import util.LoggingInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Stateless
@Local(ClimateManagerInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class ClimateManager implements ClimateManagerInterface{

	@EJB(beanName="ClimateDataService")
	DataAccessInterface<Climate> cds;
	
	//Create instance of SLF4J logger and name is based on the class name
	Logger logger = LoggerFactory.getLogger("ClimateManager LOGGER");
     
	/**
	 * This method will add weather to the database
	 * @param Climate
	 * returns boolean
	 */
	@Override
	public boolean addWeather(Climate climate) {
		
		//call the database method that adds the weather to the database
		logger.info("Creating Climate");
		return cds.create(climate);
	}

	/**
	 * This method will get last N entered weather from database
	 * @param int n
	 * @param Climate 
	 * returns List<Climate>
	 */
	@Override
	public List<Climate> getLastN(int n, Climate climate) throws ClimateNotFoundException {
		
		//set the list of climates to the dao method that return last n climates from database
		//n will be 5
		climate.setClimates(cds.findLastN(n));
		logger.info("Finding last n Climates");

		//create new list and set it equal to the returned one
		List<Climate> climateList = climate.getClimates();
		logger.info("Adding the climates in Climate List");

		//if the new list is empty throw the exception otherwise return the new list
		if(climateList == null)
			throw new ClimateNotFoundException();
			logger.info("Climate list is not empty");
		return climateList;
	}
	

}

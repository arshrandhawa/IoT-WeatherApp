package beans;

import javax.faces.bean.ManagedBean;

/**
 * @author Caryssa Moffet
 */
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ManagedBean
@ViewScoped
public class Login {

	int ID;

	@NotNull(message = "Please enter an email.")
	@Size(min = 2, max = 30, message = "Length must be greater than 2 or less than 30.")
	String email;

	@NotNull(message = "Please enter a password.")
	@Size(min = 2, max = 30, message = "Length must be greater than 2 or less than 30.")
	String password;

	public Login() {
		ID = 0;
		email = "";
		password = "";
	}

	public Login(int a, String b, String c) {
		this.ID = a;
		this.email = b;
		this.password = c;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

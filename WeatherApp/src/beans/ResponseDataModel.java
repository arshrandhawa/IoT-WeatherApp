package beans;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseDataModel extends ResponseModel implements IData{

	List<Climate> data;

	public ResponseDataModel() {
		super(0, "");
		this.data = null;
	}

	public ResponseDataModel(int status, String message, List<Climate> data) {
		super(status, message);
		this.data = data;
	}

	public List<Climate> getData() {
		return data;
	}

	public void setData(List<Climate> data) {
		this.data = data;
	}


}

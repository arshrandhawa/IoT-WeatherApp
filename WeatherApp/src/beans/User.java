package beans;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * 
 * @author Caryssa Moffet 
 */
@ManagedBean
@ViewScoped
public class User extends Login {

	@NotNull(message = "Please enter a first name.")
	@Size(min = 2, max = 30, message = "Length must be greater than 2 or less than 30.")
	String firstName;

	@NotNull(message = "Please enter a last name.")
	@Size(min = 2, max = 30, message = "Length must be greater than 2 or less than 30.")
	String lastName;

	@NotNull(message = "Please enter a location.")
	@Size(min = 2, max = 30, message = "Length must be greater than 2 or less than 30.")
	String location;

	public User() {

		firstName = "";
		lastName = "";
		location = "";
	}

	public User(int a, String b, String c, String d, String e, String f) {
		this.setID(a);
		this.setEmail(b);
		this.setPassword(c);
		this.firstName = d;
		this.lastName = e;
		this.location = f;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

}

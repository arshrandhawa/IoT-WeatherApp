package factory;

import java.util.List;

import beans.Climate;
import beans.IData;
import beans.ResponseDataModel;
import beans.ResponseModel;

public class ResponseFactory {

	public IData getResponse(String responseType, int num, String message, List<Climate> climate) {
		if (responseType == null) {
			return null;
		}
		if (responseType.equalsIgnoreCase("ResponseModel")) {
			return new ResponseModel(num, message);

		} else if (responseType.equalsIgnoreCase("ResponseDataModel")) {
			return new ResponseDataModel(num, message, climate);

		}
		return null;
	}
}

package services;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Climate;
import beans.IData;
import business.ClimateManagerInterface;
import factory.ResponseFactory;
import util.ClimateNotFoundException;

@Path("/weatherService")
public class ClimateService {

	@Inject
	ClimateManagerInterface cm;
	
	ResponseFactory rf = new ResponseFactory();
	//Create instance of SLF4J logger and name is based on the class name
	Logger logger = LoggerFactory.getLogger("ClimateService LOGGER");
	
	@GET
	@Path("/weather")
	@Produces("application/json")
	public IData getWeather() {
		// TODO: Add the DTO code
		List<Climate> climates = new ArrayList<Climate>();
		try {
			// find album using a music business service
			climates = cm.getLastN(5, new Climate());
			logger.info("Get last 5 climates from database");
			// create a response model as a return
			
		      IData data = rf.getResponse("ResponseDataModel", 0, "", climates);
			logger.info("Success in getting last 5 climates");
			
			return data;
		} catch (ClimateNotFoundException e1) {
			// create a response model with error as a return
			IData data = rf.getResponse("ResponseDataModel", -1, "Climate not found", climates);
			logger.info("Failed to find the last 5 climates");
			return data;
		} catch (Exception e2) {
			// create a response model with error as a return
			IData data = rf.getResponse("ResponseDataModel", -2, "System Exception", climates);
			logger.info("Failed to find last 5 climates because of database");
			return data;
		}
	}

	
	@POST
	@Path("/weatherArrive")
	@Consumes("application/json")
	@Produces("application/json")
	public IData saveWeather(Climate climate) throws ClimateNotFoundException{

		try {
			// find album using a music business service
			 cm.addWeather(climate);
			 logger.info("Add climate to the database");
			 //Printing on console to check what information is going to the database
			 System.out.println("Location " + climate.getLocation() + " Temp " + climate.getTemperature() + " Wind "
						+ climate.getWind() + " pressure " + climate.getPressure() + " Humidity " + climate.getHumidity());
				
			// create a response model as a return
			IData data = rf.getResponse("ResponseModel", 0, "", new ArrayList<Climate>());
			logger.info("Success in adding to database");
			return data;
		} catch (ClimateNotFoundException e1) {
			// create a response model with error as a return
			IData data = rf.getResponse("ResponseModel", -1, "Climate not found", new ArrayList<Climate>());
			logger.info("Failed to add to database");
			return data;
		} catch (Exception e2) {
			// create a response model with error as a return
			IData data = rf.getResponse("ResponseModel", -2, "System Exception", new ArrayList<Climate>());
			logger.info("Failed because of database");
			return data;
		}
		
	}

}

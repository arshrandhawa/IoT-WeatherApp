package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.User;
import util.DatabaseException;
import util.LoggingInterceptor;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class UserDataService implements DataAccessInterface<User> {

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findBy(User user) {
		// DB Connection info
				Connection conn = null;
				String url = "jdbc:mysql://localhost:8889/weather";
				String username = "root";
				String password = "root";

				// Get all Albums and Tracks
				try {
					// Connect to the Database
					conn = DriverManager.getConnection(url, username, password);

					// Execute SQL Query and loop over result set
					String sql1 = String.format(
							"SELECT * FROM USER WHERE EMAIL = '%s' AND PASSWORD = '%s'",
							user.getEmail(), user.getPassword());
					Statement stmt1 = conn.createStatement();
					ResultSet rs1 = stmt1.executeQuery(sql1);

					if (!rs1.next()) {
						rs1.close();
						stmt1.close();
						return null;
					}

					rs1.close();
					stmt1.close();
				} catch (SQLException e) {
					e.printStackTrace();
					throw new DatabaseException(e);
				} finally {
					// Cleanup Database
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							throw new DatabaseException(e);
						}
					}
				}
				return user;
	}

	@Override
	public boolean create(User user) {
		// DB Connection info
		Connection conn = null;
		String url = "jdbc:mysql://localhost:8889/weather";
		String username = "root";
		String password = "root";

		// Get all Albums and Tracks
		try {
			// Connect to the Database
			conn = DriverManager.getConnection(url, username, password);

			// Execute SQL Query and loop over result set
			String sql1 = String.format(
					"INSERT INTO USER (FIRSTNAME, LASTNAME, EMAIL, PASSWORD, LOCATION) VALUES('%s', '%s', '%s', '%s', '%s')",
					user.getFirstName(), user.getLastName(), user.getEmail(), user.getPassword(), user.getLocation());
			Statement stmt1 = conn.createStatement();
			stmt1.executeUpdate(sql1);

			stmt1.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DatabaseException(e);
		} finally {
			// Cleanup Database
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					throw new DatabaseException(e);
				}
			}
		}
		return true;
	}

	@Override
	public boolean update(User t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(User t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<User> findLastN(int n) {
		// TODO Auto-generated method stub
		return null;
	}

}

package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;

import beans.Climate;
import util.DatabaseException;
import util.LoggingInterceptor;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
@Interceptors(LoggingInterceptor.class)
public class ClimateDataService implements DataAccessInterface<Climate> {

	
	@Override
	public List<Climate> findAll() {
		
		return null;
	}

	@Override
	public Climate findById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Climate findBy(Climate t) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean create(Climate climate) {
				// DB Connection info
				Connection conn = null;
				String url = "jdbc:mysql://localhost:8889/weather";
				String username = "root";
				String password = "root";

				// Get all Albums and Tracks
				try {
					// Connect to the Database
					conn = DriverManager.getConnection(url, username, password);

					// Execute SQL Query and loop over result set
					
					
					String sql1 = String.format("INSERT INTO WEATHER (`LOCATION`, `TEMPERATURE`, `WIND`, `PRESSURE`, `HUMIDITY`, `DATETIME`) VALUES ('%s', %f, %f, %f, %f, '%s')", 
							climate.getLocation(), climate.getTemperature(), climate.getWind(), climate.getPressure(),climate.getHumidity(), new Date());
					
					// Return result of Insert
					Statement stmt1 = conn.createStatement();
					stmt1.executeUpdate(sql1);

					// Get Auto-Increment PK back
					// String sql2 = "SELECT LAST_INSERT_ID() AS LAST_ID FROM ALBUM";
					// ResultSet rs = stmt1.executeQuery(sql2);

					// rs.next();
					// String albumId = rs.getString("LAST_ID");
					// rs.close();
					stmt1.close();

				} catch (SQLException e) {
					e.printStackTrace();
					throw new DatabaseException(e);
				} finally {
					// Cleanup Database
					if (conn != null) {
						try {
							conn.close();
						} catch (SQLException e) {
							e.printStackTrace();
							throw new DatabaseException(e);
						}
					}
				}
				return true;
	}

	@Override
	public boolean update(Climate t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean delete(Climate t) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Climate> findLastN(int n) {
		// DB Connection info
		Connection conn = null;
		String url = "jdbc:mysql://localhost:8889/weather";
		String username = "root";
		String password = "root";

		List<Climate> climates = new ArrayList<Climate>();

		try {
			// Connect to the Database
			conn = DriverManager.getConnection(url, username, password);

			// Execute SQL Query and loop over result set
			
			
			String sql1 = String.format("SELECT * FROM weather order by id asc limit " + n);
			
			// Return result of Insert
			Statement stmt1 = conn.createStatement();
			stmt1.executeQuery(sql1);

			ResultSet rs1 = stmt1.executeQuery(sql1);
			while (rs1.next()) {
				// get climate 
				Climate climate = new Climate(rs1.getString("LOCATION"), rs1.getFloat("TEMPERATURE"), rs1.getFloat("WIND"), rs1.getString("DATETIME"), rs1.getFloat("PRESSURE"),rs1.getFloat("HUMIDITY"));
				
				climates.add(climate);
			}
			
			//clean up
			rs1.close();
			stmt1.close();

		} catch (SQLException e) {
			e.printStackTrace();
			throw new DatabaseException(e);
		} finally {
			// Cleanup Database
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					throw new DatabaseException(e);
				}
			}
		}
		return climates;
	}

}

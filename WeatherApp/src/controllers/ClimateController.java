package controllers;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.Climate;
import business.ClimateManagerInterface;
import util.ClimateNotFoundException;
import util.LoggingInterceptor;

@Interceptors(LoggingInterceptor.class)
@Named
@ViewScoped
public class ClimateController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	ClimateManagerInterface cm;

	//Create instance of SLF4J logger and name is based on the class name
	Logger logger = LoggerFactory.getLogger("ClimateController LOGGER");

	/*
	 * This class returns last 5 climates in the form of table
	 */
	public String lastNClimates() {

		// Get the climate managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		Climate climate = context.getApplication().evaluateExpressionGet(context, "#{climate}", Climate.class);

		try {
			// pass 5 to get last 5 climates
			cm.getLastN(5, climate);
			logger.info("getting last 5 climates and returning them to xHTML page");
		} catch (ClimateNotFoundException e) {
			// return exception on console and goes to other screen
			System.out.println("----------------> CLIMATE NOT FOUND <----------------");
			logger.info("Failed to find the last 5 climates");
			return "Exception.xhtml";
		}

		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("climate", climate);

		logger.info("success in sending information to xhtml page");
		return "DisplayChart.xhtml";
	}
	
	/*
	 * Line chart is created by the information sent to the LineChart page by API in ClimateService class
	 */
	public String lineChart() {
		// Get the climate managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		Climate climate = context.getApplication().evaluateExpressionGet(context, "#{climate}", Climate.class);

		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("climate", climate);

		logger.info("success reaching Line chart page");
		return "LineChart.xhtml";
	}
}

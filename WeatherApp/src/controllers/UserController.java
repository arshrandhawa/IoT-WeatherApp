package controllers;

import java.io.Serializable;

import javax.ejb.EJB;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.interceptor.Interceptors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import beans.User;
import business.UserManagerInterface;
import util.LoggingInterceptor;
import util.UserAlreadyExistException;
import util.UserDoesNotExistException;

@Interceptors(LoggingInterceptor.class)
@Named
@ViewScoped
public class UserController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	UserManagerInterface ugr;
	
	//Create instance of SLF4J logger and name is based on the class name
	Logger logger = LoggerFactory.getLogger("UserControlelr LOGGER");

	public String loginUser() {

		// Get the user managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		try {
			ugr.loginUser(user);
			logger.info("user logged in");
		} catch (UserDoesNotExistException e) {
			
			//return exception on console and goes to other screen
			System.out.println("----------------> User Does Not Exist <----------------");
			logger.info("Login Failed");
			return "Exception.xhtml";
		}
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);
		return "Homepage.xhtml";
	}

	public String registerUser() {

		// Get the user managed bean
		FacesContext context = FacesContext.getCurrentInstance();
		User user = context.getApplication().evaluateExpressionGet(context, "#{user}", User.class);

		try {
			ugr.addUser(user);
			logger.info("User added to the database and logged in");
		} catch (UserAlreadyExistException e) {
			
			//return exception on console and goes to other screen
			System.out.println("----------------> User Already Exist <----------------");
			logger.info("Resgiteration Failed");
			return "Exception.xhtml";
		}
		FacesContext.getCurrentInstance().getExternalContext().getRequestMap().put("user", user);

		return "Homepage.xhtml";
	}
}
